package com.my.banktransferapp.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.my.banktransferapp.dto.TransactionHistoryResDto;
import com.my.banktransferapp.dto.TransactionSuccessResDto;
import com.my.banktransferapp.dto.TransferPhoneNoRequestDto;
import com.my.banktransferapp.dto.TransferReqDto;
import com.my.banktransferapp.exception.AccountException;
import com.my.banktransferapp.exception.AccountNotFoundException;
import com.my.banktransferapp.service.TransactionService;

@RestController
public class TransactionController {

	
	@Autowired
	TransactionService transactionService;
	
	@GetMapping("/statement/account-number/{accountNumber}/month/{month}/year/{year}")
	public ResponseEntity<List<TransactionHistoryResDto>> getStatementOfAccount(@PathVariable @NotBlank @NotEmpty Long accountNumber, @NotBlank @NotEmpty String month, @NotBlank @NotEmpty String year) {
		
		List<TransactionHistoryResDto> statementOfAccount = transactionService.getStatementOfAccount(accountNumber, month, year);		
		return ResponseEntity.ok().header("desc", "Monthly Statement Of the Given Account").body(statementOfAccount);
	}
	
	@PutMapping("/amount-transfer")
	public ResponseEntity<TransactionSuccessResDto> transferedAmount(@Valid @RequestBody TransferReqDto transferBalanceRequest) throws AccountException, AccountNotFoundException {		
		TransactionSuccessResDto transactionSuccessRes = transactionService.transferAmount(transferBalanceRequest);
		HttpHeaders header = new HttpHeaders();
		header.add("desc", "Fund has been Transferred successfully");
		return ResponseEntity.status(HttpStatus.OK).headers(header).body(transactionSuccessRes);
	}
	
	@PutMapping("/phone-transfer")
	public ResponseEntity<TransactionSuccessResDto> transferedAmountByPhoneNumber(@Valid @RequestBody TransferPhoneNoRequestDto transferBalanceRequest) throws AccountException, AccountNotFoundException {		
		TransactionSuccessResDto transactionSuccessRes = transactionService.transferAmountWithPhoneNumber(transferBalanceRequest);
		HttpHeaders header = new HttpHeaders();
		header.add("desc", "Fund has been Transferred successfully");
		return ResponseEntity.status(HttpStatus.OK).headers(header).body(transactionSuccessRes);
	}
	
	
}

