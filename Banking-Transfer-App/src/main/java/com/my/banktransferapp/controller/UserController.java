package com.my.banktransferapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.my.banktransferapp.dto.UserRegistrationDto;
import com.my.banktransferapp.dto.UserPhoneValidationResDto;
import com.my.banktransferapp.model.User;
import com.my.banktransferapp.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/register")
	public ResponseEntity<User> userRegistration(@Valid @RequestBody UserRegistrationDto userRegistrationDto) {	
		User createdUser = userService.registerNewUser(userRegistrationDto);
		return ResponseEntity.status(HttpStatus.CREATED).header("desc", "Successfully created new user").body(createdUser);
	}
	
	@GetMapping("/users/phonenumber/{phoneNumber}/validate")
	public ResponseEntity<UserPhoneValidationResDto> userValidationWithPhoneNumber(@PathVariable String phoneNumber) {
		UserPhoneValidationResDto userValidationResponse = userService.validateUserByPhoneNumber(phoneNumber);
		return ResponseEntity.status(HttpStatus.OK).body(userValidationResponse);
	}

}
