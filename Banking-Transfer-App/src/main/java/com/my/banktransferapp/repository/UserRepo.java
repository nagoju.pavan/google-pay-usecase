package com.my.banktransferapp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.my.banktransferapp.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
	public Optional<User> findByPhoneNumber(String phoneNumber);

}
