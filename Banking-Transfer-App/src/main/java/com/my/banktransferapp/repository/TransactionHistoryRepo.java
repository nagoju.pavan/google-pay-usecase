package com.my.banktransferapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.my.banktransferapp.model.TransactionHistory;

@Repository
public interface TransactionHistoryRepo extends JpaRepository<TransactionHistory, Long> {
	
	
	public List<TransactionHistory> findByUserAccountNumber(Long userAccountNumber);
	
	@Query("select t from TransactionHistory t where t.userAccountNumber = ?1 and  month(t.transactionDate) = ?2 and year(t.transactionDate) = ?3")
	public List<TransactionHistory> getStatementOfAccount(Long accountNumber, int month, int year);
	
}
