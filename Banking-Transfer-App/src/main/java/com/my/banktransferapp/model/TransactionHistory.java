package com.my.banktransferapp.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class TransactionHistory {

	/**
	 * 
	 */
	public TransactionHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue
	private Long transactionID;

	private Long userAccountNumber;

	private Long otherAccountNumber;

	private BigDecimal transactionAmount;
	private String transactionType;
	@Column
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date transactionDate;
	private String comment;

	/**
	 * @return the transactionID
	 */
	public Long getTransactionID() {
		return transactionID;
	}

	/**
	 * @param transactionID the transactionID to set
	 */
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}

	/**
	 * @return the userAccountNumber
	 */
	public Long getUserAccountNumber() {
		return userAccountNumber;
	}

	/**
	 * @param userAccountNumber the userAccountNumber to set
	 */
	public void setUserAccountNumber(Long userAccountNumber) {
		this.userAccountNumber = userAccountNumber;
	}

	/**
	 * @return the otherAccountNumber
	 */
	public Long getOtherAccountNumber() {
		return otherAccountNumber;
	}

	/**
	 * @param otherAccountNumber the otherAccountNumber to set
	 */
	public void setOtherAccountNumber(Long otherAccountNumber) {
		this.otherAccountNumber = otherAccountNumber;
	}

	/**
	 * @return the transactionAmount
	 */
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}

	/**
	 * @param transactionAmount the transactionAmount to set
	 */
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}

	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @param transactionID
	 * @param userAccountNumber
	 * @param otherAccountNumber
	 * @param transactionAmount
	 * @param transactionType
	 * @param transactionDate
	 * @param comment
	 */
	public TransactionHistory(Long transactionID, Long userAccountNumber, Long otherAccountNumber,
			BigDecimal transactionAmount, String transactionType, Date transactionDate, String comment) {
		super();
		this.transactionID = transactionID;
		this.userAccountNumber = userAccountNumber;
		this.otherAccountNumber = otherAccountNumber;
		this.transactionAmount = transactionAmount;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "TransactionHistory [transactionID=" + transactionID + ", userAccountNumber=" + userAccountNumber
				+ ", otherAccountNumber=" + otherAccountNumber + ", transactionAmount=" + transactionAmount
				+ ", transactionType=" + transactionType + ", transactionDate=" + transactionDate + ", comment="
				+ comment + "]";
	}

}
