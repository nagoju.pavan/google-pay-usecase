package com.my.banktransferapp.service;

import java.math.BigDecimal;
import java.util.List;

import com.my.banktransferapp.exception.AccountNotFoundException;
import com.my.banktransferapp.model.Account;

public interface AccountService {
	
	public List<Account> getAllAccounts();
	public BigDecimal getAccountBalance(Long accountNumber) throws AccountNotFoundException;

}
