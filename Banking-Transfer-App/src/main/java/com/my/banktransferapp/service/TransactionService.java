package com.my.banktransferapp.service;


import java.util.List;

import com.my.banktransferapp.dto.TransactionHistoryResDto;
import com.my.banktransferapp.dto.TransactionSuccessResDto;
import com.my.banktransferapp.dto.TransferPhoneNoRequestDto;
import com.my.banktransferapp.dto.TransferReqDto;
import com.my.banktransferapp.exception.AccountException;
import com.my.banktransferapp.exception.AccountNotFoundException;

public interface TransactionService {
	
	public TransactionSuccessResDto transferAmount(TransferReqDto transferBalanceRequest) throws AccountException, AccountNotFoundException;
	public List<TransactionHistoryResDto> getStatementOfAccount(Long accountNumber, String month, String year);
	public TransactionSuccessResDto transferAmountWithPhoneNumber(TransferPhoneNoRequestDto transferAmountRequest);
}
