package com.my.banktransferapp.service.impl;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.banktransferapp.dto.UserRegistrationDto;
import com.my.banktransferapp.dto.UserPhoneValidationResDto;
import com.my.banktransferapp.exception.UserNotFoundException;
import com.my.banktransferapp.model.Account;
import com.my.banktransferapp.model.User;
import com.my.banktransferapp.model.TransactionHistory;
import com.my.banktransferapp.repository.AccountRepo;
import com.my.banktransferapp.repository.UserRepo;
import com.my.banktransferapp.repository.TransactionHistoryRepo;
import com.my.banktransferapp.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepo userRepository;

	@Autowired
	AccountRepo accountRepository;
	
	@Autowired
	TransactionHistoryRepo transactionRepository;
	
	@Transactional
	public User registerNewUser(UserRegistrationDto userRegistrationDto) {
		
		User user = new User();
		BeanUtils.copyProperties(userRegistrationDto, user);
		
		Account account = new Account();
		BeanUtils.copyProperties(userRegistrationDto.getAccountRegistrationDto(), account);
		account.setUser(user);
		account = accountRepository.save(account);
		
		user = userRepository.save(user);
		TransactionHistory initialDeposit = new TransactionHistory(0L, account.getAccountNumber(), account.getAccountNumber(), account.getCurrentBalance(), 
				"Account Opening Initial Deposit", new Date(), "Deposit while opening the account");
			transactionRepository.save(initialDeposit);
		
		return user;
	}

	@Override
	public UserPhoneValidationResDto validateUserByPhoneNumber(String phoneNumber) {
		User user = userRepository.findByPhoneNumber(phoneNumber).orElseThrow( () -> new UserNotFoundException("No Customer found with the given phone number"));
		
		UserPhoneValidationResDto customerValidationResponse = new UserPhoneValidationResDto();
		
		Boolean isCustomerFound = user.getPhoneNumber().equals(phoneNumber) ? true : false;
		customerValidationResponse.setDoesUserExists(isCustomerFound);
	
		return customerValidationResponse;
	}
	

}
