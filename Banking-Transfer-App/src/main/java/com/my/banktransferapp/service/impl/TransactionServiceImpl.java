package com.my.banktransferapp.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.banktransferapp.dto.TransactionHistoryResDto;
import com.my.banktransferapp.dto.TransactionSuccessResDto;
import com.my.banktransferapp.dto.TransferPhoneNoRequestDto;
import com.my.banktransferapp.dto.TransferReqDto;
import com.my.banktransferapp.exception.AccountException;
import com.my.banktransferapp.exception.AccountNotFoundException;
import com.my.banktransferapp.model.Account;
import com.my.banktransferapp.model.TransactionHistory;
import com.my.banktransferapp.repository.AccountRepo;
import com.my.banktransferapp.repository.TransactionHistoryRepo;
import com.my.banktransferapp.service.TransactionService;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

	private static final String PHONE_NUMBER_TRANSACTION = "Transaction Via Phone Number: ";

	@Autowired
	TransactionHistoryRepo transactionRepository;
	
	@Autowired
	AccountRepo accountRepository;
	
	public TransactionSuccessResDto transferAmount(TransferReqDto transferBalanceRequest) throws AccountException, AccountNotFoundException{
		Long fromAccountNumber = transferBalanceRequest.getFromAccountNumber();
		Long toAccountNumber = transferBalanceRequest.getToAccountNumber();
		BigDecimal amountToBeTransferred = transferBalanceRequest.getAmountToBeTransferred();
		
		Account fromAccount = accountRepository.findById(fromAccountNumber).
				orElseThrow(() -> new AccountNotFoundException("No Account exists with Account Number: " + fromAccountNumber));
		Account toAccount = accountRepository.findById(toAccountNumber).
				orElseThrow(() -> new AccountNotFoundException("No Account exists with Account Number: " + toAccountNumber));
		
		processAmountTransfer(amountToBeTransferred, fromAccount, toAccount);
		
		return createTransactionHistory(fromAccountNumber, toAccountNumber, amountToBeTransferred, transferBalanceRequest.getDateOfTransaction(),transferBalanceRequest.getComments());
		
		
	}

	public List<TransactionHistoryResDto> getStatementOfAccount(Long accountNumber, String month, String year) {
		List<TransactionHistory> transactionHistoryList = transactionRepository.getStatementOfAccount(accountNumber,Integer.valueOf(month),Integer.valueOf(year));
		return transactionHistoryList.stream().map(transactionHistory -> {
			TransactionHistoryResDto transactionHistoryDto = new TransactionHistoryResDto();
			BeanUtils.copyProperties(transactionHistory, transactionHistoryDto);
			return transactionHistoryDto;
		}).collect(Collectors.toList());
	}
	

	@Override
	public TransactionSuccessResDto transferAmountWithPhoneNumber(
			TransferPhoneNoRequestDto transferAmountRequest) {
		String fromPhoneNumber = transferAmountRequest.getFromPhoneNumber();
		String toPhoneNumber = transferAmountRequest.getToPhoneNumber();
		BigDecimal amountToBeTransferred = transferAmountRequest.getAmountToBeTransferred();
		
		Account fromAccount = accountRepository.findByUser_phoneNumber(fromPhoneNumber).
				orElseThrow(() -> new AccountNotFoundException("No Account exists with Account Number: " + fromPhoneNumber));
		Account toAccount = accountRepository.findByUser_phoneNumber(toPhoneNumber).
				orElseThrow(() -> new AccountNotFoundException("No Account exists with Account Number: " + toPhoneNumber));
		
		processAmountTransfer(amountToBeTransferred, fromAccount, toAccount);
		
		return createTransactionHistory(fromAccount.getAccountNumber(), toAccount.getAccountNumber(), amountToBeTransferred, 
					transferAmountRequest.getDateOfTransaction(),PHONE_NUMBER_TRANSACTION + transferAmountRequest.getComments());

	}
	
	private TransactionSuccessResDto createTransactionHistory(Long fromAccountNumber,
			Long toAccountNumber, BigDecimal amountToBeTransferred, Date dateOfTransaction, String comment) {
		TransactionHistory debitTransaction = new TransactionHistory(0L, fromAccountNumber, toAccountNumber, amountToBeTransferred, 
																		"DEBIT", dateOfTransaction, comment);
		debitTransaction = transactionRepository.save(debitTransaction);
		
		TransactionHistory creditTransaction = new TransactionHistory(0L, toAccountNumber, fromAccountNumber, amountToBeTransferred, 
																		"CREDIT", dateOfTransaction, comment);
		creditTransaction = transactionRepository.save(creditTransaction);
		
		TransactionSuccessResDto transactionResponse = new TransactionSuccessResDto(debitTransaction.getTransactionID(), 
																	creditTransaction.getTransactionID());
		return transactionResponse;
	}
	
	private void processAmountTransfer(BigDecimal amountToBeTransferred, Account fromAccount, Account toAccount) {
		if(fromAccount.getCurrentBalance().compareTo(amountToBeTransferred) < 0) {
			throw new AccountException("Insufficient balance to make this transaction");		
		}
		
		fromAccount.setCurrentBalance(fromAccount.getCurrentBalance().subtract(amountToBeTransferred));
		accountRepository.save(fromAccount);
		toAccount.setCurrentBalance(toAccount.getCurrentBalance().add(amountToBeTransferred));		
		accountRepository.save(toAccount);
	}
}
