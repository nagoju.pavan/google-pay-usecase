package com.my.banktransferapp.service;

import com.my.banktransferapp.dto.UserRegistrationDto;
import com.my.banktransferapp.dto.UserPhoneValidationResDto;
import com.my.banktransferapp.model.User;

public interface UserService {
	
public User registerNewUser(UserRegistrationDto userRegistrationDto);

public UserPhoneValidationResDto validateUserByPhoneNumber(String phoneNumber);

}
