package com.my.banktransferapp.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.banktransferapp.exception.AccountNotFoundException;
import com.my.banktransferapp.model.Account;
import com.my.banktransferapp.repository.AccountRepo;
import com.my.banktransferapp.service.AccountService;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountRepo accountRepository;
	
	public List<Account> getAllAccounts() {
		return accountRepository.findAll();
	}
	
	public BigDecimal getAccountBalance(Long accountNumber) throws AccountNotFoundException {
		Account account = accountRepository.findById(accountNumber).
				orElseThrow(() -> new AccountNotFoundException("No Account exists with Account Number: " + accountNumber));
		return account.getCurrentBalance(); 
	}
	
}
