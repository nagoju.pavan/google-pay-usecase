package com.my.banktransferapp.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;

public class TransferPhoneNoRequestDto {

	@Min(value = 1, message = "Invalid Phone Number")
	private String fromPhoneNumber;
	private String toPhoneNumber;
	@Min(value = 1, message = "Invalid Fund to transfer")
	private BigDecimal amountToBeTransferred;
	private Date dateOfTransaction;
	private String comments;

	/**
	 * @return the fromPhoneNumber
	 */
	public String getFromPhoneNumber() {
		return fromPhoneNumber;
	}

	/**
	 * @param fromPhoneNumber the fromPhoneNumber to set
	 */
	public void setFromPhoneNumber(String fromPhoneNumber) {
		this.fromPhoneNumber = fromPhoneNumber;
	}

	/**
	 * @return the toPhoneNumber
	 */
	public String getToPhoneNumber() {
		return toPhoneNumber;
	}

	/**
	 * @param toPhoneNumber the toPhoneNumber to set
	 */
	public void setToPhoneNumber(String toPhoneNumber) {
		this.toPhoneNumber = toPhoneNumber;
	}

	/**
	 * @return the amountToBeTransferred
	 */
	public BigDecimal getAmountToBeTransferred() {
		return amountToBeTransferred;
	}

	/**
	 * @param amountToBeTransferred the amountToBeTransferred to set
	 */
	public void setAmountToBeTransferred(BigDecimal amountToBeTransferred) {
		this.amountToBeTransferred = amountToBeTransferred;
	}

	/**
	 * @return the dateOfTransaction
	 */
	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}

	/**
	 * @param dateOfTransaction the dateOfTransaction to set
	 */
	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

}
