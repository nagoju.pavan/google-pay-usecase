package com.my.banktransferapp.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class AccountUpdateDto {
	
	@NotNull(message = "Account Details are Mandatory")
	@Min(1)
	private long accountNumber;
	@NotNull(message = "please specify the amount to transfer")
	@Min(1)
	private BigDecimal addMoneyToAccount;
	
	/**
	 * @return the accountNumber
	 */
	public long getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the addMoneyToAccount
	 */
	public BigDecimal getAddMoneyToAccount() {
		return addMoneyToAccount;
	}
	/**
	 * @param addMoneyToAccount the addMoneyToAccount to set
	 */
	public void setAddMoneyToAccount(BigDecimal addMoneyToAccount) {
		this.addMoneyToAccount = addMoneyToAccount;
	}
	
	

	
}
