package com.my.banktransferapp.dto;

public class UserPhoneValidationResDto {
	
	public Boolean doesUserExists;

	/**
	 * @return the doesUserExists
	 */
	public Boolean getDoesUserExists() {
		return doesUserExists;
	}

	/**
	 * @param doesUserExists the doesUserExists to set
	 */
	public void setDoesUserExists(Boolean doesUserExists) {
		this.doesUserExists = doesUserExists;
	}

	
	
	

}
