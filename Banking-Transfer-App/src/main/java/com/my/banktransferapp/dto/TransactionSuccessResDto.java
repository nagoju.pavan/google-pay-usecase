package com.my.banktransferapp.dto;

public class TransactionSuccessResDto {

	Long debitTransactionID;
	Long creditTransactionID;

	/**
	 * @param debitTransactionID
	 * @param creditTransactionID
	 */
	public TransactionSuccessResDto(Long debitTransactionID, Long creditTransactionID) {
		super();
		this.debitTransactionID = debitTransactionID;
		this.creditTransactionID = creditTransactionID;
	}

	/**
	 * @return the debitTransactionID
	 */
	public Long getDebitTransactionID() {
		return debitTransactionID;
	}

	/**
	 * @param debitTransactionID the debitTransactionID to set
	 */
	public void setDebitTransactionID(Long debitTransactionID) {
		this.debitTransactionID = debitTransactionID;
	}

	/**
	 * @return the creditTransactionID
	 */
	public Long getCreditTransactionID() {
		return creditTransactionID;
	}

	/**
	 * @param creditTransactionID the creditTransactionID to set
	 */
	public void setCreditTransactionID(Long creditTransactionID) {
		this.creditTransactionID = creditTransactionID;
	}

}
