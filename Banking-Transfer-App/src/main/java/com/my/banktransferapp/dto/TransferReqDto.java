package com.my.banktransferapp.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;

public class TransferReqDto {

	@Min(value = 1, message = "Invalid Account Details")
	private Long fromAccountNumber;
	@Min(value = 1, message = "Invalid Account Details")
	private Long toAccountNumber;
	@Min(value = 1, message = "Invalid Fund to transfer")
	private BigDecimal amountToBeTransferred;
	private Date dateOfTransaction;
	private String comments;

	/**
	 * @return the fromAccountNumber
	 */
	public Long getFromAccountNumber() {
		return fromAccountNumber;
	}

	/**
	 * @param fromAccountNumber the fromAccountNumber to set
	 */
	public void setFromAccountNumber(Long fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}

	/**
	 * @return the toAccountNumber
	 */
	public Long getToAccountNumber() {
		return toAccountNumber;
	}

	/**
	 * @param toAccountNumber the toAccountNumber to set
	 */
	public void setToAccountNumber(Long toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}

	/**
	 * @return the amountToBeTransferred
	 */
	public BigDecimal getAmountToBeTransferred() {
		return amountToBeTransferred;
	}

	/**
	 * @param amountToBeTransferred the amountToBeTransferred to set
	 */
	public void setAmountToBeTransferred(BigDecimal amountToBeTransferred) {
		this.amountToBeTransferred = amountToBeTransferred;
	}

	/**
	 * @return the dateOfTransaction
	 */
	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}

	/**
	 * @param dateOfTransaction the dateOfTransaction to set
	 */
	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

}
