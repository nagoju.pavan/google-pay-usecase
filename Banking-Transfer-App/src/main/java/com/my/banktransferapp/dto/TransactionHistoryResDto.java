package com.my.banktransferapp.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TransactionHistoryResDto {

	private Long transactionID;
	private Long userAccountNumber;
	private Long otherAccountNumber;
	private BigDecimal transactionAmount;
	private String transactionType;
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date transactionDate;
	private String comments;
	
	/**
	 * @return the transactionID
	 */
	public Long getTransactionID() {
		return transactionID;
	}
	/**
	 * @param transactionID the transactionID to set
	 */
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}
	/**
	 * @return the userAccountNumber
	 */
	public Long getUserAccountNumber() {
		return userAccountNumber;
	}
	/**
	 * @param userAccountNumber the userAccountNumber to set
	 */
	public void setUserAccountNumber(Long userAccountNumber) {
		this.userAccountNumber = userAccountNumber;
	}
	/**
	 * @return the otherAccountNumber
	 */
	public Long getOtherAccountNumber() {
		return otherAccountNumber;
	}
	/**
	 * @param otherAccountNumber the otherAccountNumber to set
	 */
	public void setOtherAccountNumber(Long otherAccountNumber) {
		this.otherAccountNumber = otherAccountNumber;
	}
	/**
	 * @return the transactionAmount
	 */
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	/**
	 * @param transactionAmount the transactionAmount to set
	 */
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	

}
