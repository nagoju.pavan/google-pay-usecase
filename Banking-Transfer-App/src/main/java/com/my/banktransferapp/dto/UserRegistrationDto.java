package com.my.banktransferapp.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class UserRegistrationDto {
	
    @NotBlank(message = "First Name is a mandatory")
	private String firstName;
    @NotBlank(message = "Last name is a mandatory")
	private String lastName;
    @Min(value = 13, message = "Minimum age should be 13")
    @Max(value = 100, message = "Maximum age is 100")
    private int age;
    @NotBlank(message = "Address is a mandatory")
    private String address;
    @NotBlank(message = "User Phone Number is a mandatory")
	private String phoneNumber;
    @NotBlank(message = "User email is a mandatory")
    @Email
	private String email;    
    @NotBlank(message = "uan is a mandatory")
	private String uanNumber;
    @NotBlank(message = "password is a mandatory")
    private String password;
    @Valid	
	private AccountRegistrationDto accountRegistrationDto;
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the uanNumber
	 */
	public String getUanNumber() {
		return uanNumber;
	}
	/**
	 * @param uanNumber the uanNumber to set
	 */
	public void setUanNumber(String uanNumber) {
		this.uanNumber = uanNumber;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the accountRegistrationDto
	 */
	public AccountRegistrationDto getAccountRegistrationDto() {
		return accountRegistrationDto;
	}
	/**
	 * @param accountRegistrationDto the accountRegistrationDto to set
	 */
	public void setAccountRegistrationDto(AccountRegistrationDto accountRegistrationDto) {
		this.accountRegistrationDto = accountRegistrationDto;
	}
	
	
	
}
