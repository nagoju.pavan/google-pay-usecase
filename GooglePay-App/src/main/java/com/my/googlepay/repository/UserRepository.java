package com.my.googlepay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.my.googlepay.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
	public List<User> findByPhoneNumberIn(List<String> phoneNumbers);

}
