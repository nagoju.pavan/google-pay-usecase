package com.my.googlepay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.my.googlepay.model.TransactionHistory;

import org.springframework.data.domain.Pageable;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, Long> {	
		public List<TransactionHistory> findFirstByUserPhoneNumberOrderByTransactionDateDesc(String userPhoneNumber);
		
		public List<TransactionHistory> findByUserPhoneNumberOrderByTransactionDateDesc(String userPhoneNumber, Pageable pageable);

}
