package com.my.googlepay.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.my.googlepay.dto.AccountTransactionSuccessRespDto;
import com.my.googlepay.dto.TransferAmountViaPhoneNoReqDto;
import com.my.googlepay.dto.UserValidationViaPhoneNoRespDto;

@FeignClient(name = "http://BANKING-SERVICE/banktransfer")
public interface GpayClient {
	@GetMapping("/users/phonenumber/{phoneNumber}/validate")
	public ResponseEntity<UserValidationViaPhoneNoRespDto> userValidationViaPhoneNumber(
			@PathVariable("phoneNumber") String phoneNumber);

	@PutMapping("/phone-transfer")
	public ResponseEntity<AccountTransactionSuccessRespDto> transferMoneyThroughPhoneNumber(
			@RequestBody TransferAmountViaPhoneNoReqDto transferAmountViaPhoneNumberReq);
}
