package com.my.googlepay.dto;

import java.math.BigDecimal;
import java.util.Date;

public class AccountTransactionHistoryRespDto {
	
private Long transactionID;
	
	
	private String userPhoneNumber;
	private String recepientPhoneNumber;
	private BigDecimal transactionAmount;
	private String transactionType;
	private Date transactionDate;
	private String comments;
	/**
	 * @return the transactionID
	 */
	public Long getTransactionID() {
		return transactionID;
	}
	/**
	 * @param transactionID the transactionID to set
	 */
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}
	/**
	 * @return the userPhoneNumber
	 */
	public String getUserPhoneNumber() {
		return userPhoneNumber;
	}
	/**
	 * @param userPhoneNumber the userPhoneNumber to set
	 */
	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}
	/**
	 * @return the recepientPhoneNumber
	 */
	public String getRecepientPhoneNumber() {
		return recepientPhoneNumber;
	}
	/**
	 * @param recepientPhoneNumber the recepientPhoneNumber to set
	 */
	public void setRecepientPhoneNumber(String recepientPhoneNumber) {
		this.recepientPhoneNumber = recepientPhoneNumber;
	}
	/**
	 * @return the transactionAmount
	 */
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	/**
	 * @param transactionAmount the transactionAmount to set
	 */
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @param transactionID
	 * @param userPhoneNumber
	 * @param recepientPhoneNumber
	 * @param transactionAmount
	 * @param transactionType
	 * @param transactionDate
	 * @param comments
	 */
	public AccountTransactionHistoryRespDto(Long transactionID, String userPhoneNumber, String recepientPhoneNumber,
			BigDecimal transactionAmount, String transactionType, Date transactionDate, String comments) {
		super();
		this.transactionID = transactionID;
		this.userPhoneNumber = userPhoneNumber;
		this.recepientPhoneNumber = recepientPhoneNumber;
		this.transactionAmount = transactionAmount;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.comments = comments;
	}
	
	
	

}
