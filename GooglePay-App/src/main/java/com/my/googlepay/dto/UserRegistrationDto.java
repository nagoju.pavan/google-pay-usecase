package com.my.googlepay.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class UserRegistrationDto {
	
    @NotBlank(message = "First name is a mandatory")
	private String firstName;
    @NotBlank(message = "Last name is a mandatory")
	private String lastName;
    private int age;
    @NotBlank(message = "user phone number is a mandatory")
    @Pattern(regexp = "^[0-9]{10}$",message = "Please Enter Details")
	private String phoneNumber;
    @NotBlank(message = "user email is a mandatory")
    @Email
	private String email;    
    @NotBlank(message = "password is a mandatory")
    private String password;
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param firstName
	 * @param lastName
	 * @param age
	 * @param phoneNumber
	 * @param email
	 * @param password
	 */
	public UserRegistrationDto(@NotBlank(message = "First name is a mandatory") String firstName,
			@NotBlank(message = "Last name is a mandatory") String lastName, int age,
			@NotBlank(message = "user phone number is a mandatory") @Pattern(regexp = "^[0-9]{10}$", message = "Please Enter Details") String phoneNumber,
			@NotBlank(message = "user email is a mandatory") @Email String email,
			@NotBlank(message = "password is a mandatory") String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.password = password;
	}
	/**
	 * 
	 */
	public UserRegistrationDto() {
		super();
		
	}

    
}

