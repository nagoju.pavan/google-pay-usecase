package com.my.googlepay.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;

public class TransferAmountViaPhoneNoReqDto {

	@Min(value = 1, message = "Invalid Details Found, Please Enter Correct Details")
	private String fromPhoneNumber;
	@Min(value = 1, message = "Invalid Details Found, Please Enter Correct Details")
	private String toPhoneNumber;
	@Min(value = 1, message = "Invalid Balance to transfer")
	private BigDecimal amountToBeTransferred;
	private Date dateOfTransaction;
	private String comments;
	/**
	 * @return the fromPhoneNumber
	 */
	public String getFromPhoneNumber() {
		return fromPhoneNumber;
	}
	/**
	 * @param fromPhoneNumber the fromPhoneNumber to set
	 */
	public void setFromPhoneNumber(String fromPhoneNumber) {
		this.fromPhoneNumber = fromPhoneNumber;
	}
	/**
	 * @return the toPhoneNumber
	 */
	public String getToPhoneNumber() {
		return toPhoneNumber;
	}
	/**
	 * @param toPhoneNumber the toPhoneNumber to set
	 */
	public void setToPhoneNumber(String toPhoneNumber) {
		this.toPhoneNumber = toPhoneNumber;
	}
	/**
	 * @return the amountToBeTransferred
	 */
	public BigDecimal getAmountToBeTransferred() {
		return amountToBeTransferred;
	}
	/**
	 * @param amountToBeTransferred the amountToBeTransferred to set
	 */
	public void setAmountToBeTransferred(BigDecimal amountToBeTransferred) {
		this.amountToBeTransferred = amountToBeTransferred;
	}
	/**
	 * @return the dateOfTransaction
	 */
	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}
	/**
	 * @param dateOfTransaction the dateOfTransaction to set
	 */
	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @param fromPhoneNumber
	 * @param toPhoneNumber
	 * @param amountToBeTransferred
	 * @param dateOfTransaction
	 * @param comments
	 */
	public TransferAmountViaPhoneNoReqDto(
			@Min(value = 1, message = "Invalid Details Found, Please Enter Correct Details") String fromPhoneNumber,
			@Min(value = 1, message = "Invalid Details Found, Please Enter Correct Details") String toPhoneNumber,
			@Min(value = 1, message = "Invalid Balance to transfer") BigDecimal amountToBeTransferred,
			Date dateOfTransaction, String comments) {
		super();
		this.fromPhoneNumber = fromPhoneNumber;
		this.toPhoneNumber = toPhoneNumber;
		this.amountToBeTransferred = amountToBeTransferred;
		this.dateOfTransaction = dateOfTransaction;
		this.comments = comments;
	}
	@Override
	public String toString() {
		return "TransferAmountViaPhoneNoReqDto [fromPhoneNumber=" + fromPhoneNumber + ", toPhoneNumber=" + toPhoneNumber
				+ ", amountToBeTransferred=" + amountToBeTransferred + ", dateOfTransaction=" + dateOfTransaction
				+ ", comments=" + comments + "]";
	}
	/**
	 * 
	 */
	public TransferAmountViaPhoneNoReqDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	
	
	

}
