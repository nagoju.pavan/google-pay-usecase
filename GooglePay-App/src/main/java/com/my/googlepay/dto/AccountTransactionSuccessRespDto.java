package com.my.googlepay.dto;

public class AccountTransactionSuccessRespDto {
	
	Long debitTransactionID;
	Long creditTransactionID;
	/**
	 * @return the debitTransactionID
	 */
	public Long getDebitTransactionID() {
		return debitTransactionID;
	}
	/**
	 * @param debitTransactionID the debitTransactionID to set
	 */
	public void setDebitTransactionID(Long debitTransactionID) {
		this.debitTransactionID = debitTransactionID;
	}
	/**
	 * @return the creditTransactionID
	 */
	public Long getCreditTransactionID() {
		return creditTransactionID;
	}
	/**
	 * @param creditTransactionID the creditTransactionID to set
	 */
	public void setCreditTransactionID(Long creditTransactionID) {
		this.creditTransactionID = creditTransactionID;
	}
	/**
	 * @param debitTransactionID
	 * @param creditTransactionID
	 */
	public AccountTransactionSuccessRespDto(Long debitTransactionID, Long creditTransactionID) {
		super();
		this.debitTransactionID = debitTransactionID;
		this.creditTransactionID = creditTransactionID;
	}
	/**
	 * 
	 */
	public AccountTransactionSuccessRespDto() {
		super();
		
	}
	
	
		
	

}
