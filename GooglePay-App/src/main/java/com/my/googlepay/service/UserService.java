package com.my.googlepay.service;

import javax.validation.Valid;

import com.my.googlepay.dto.UserCreatedRespDto;
import com.my.googlepay.dto.UserRegistrationDto;

public interface UserService {

	UserCreatedRespDto registerCustomer(UserRegistrationDto customerRegistrationDto);

}
