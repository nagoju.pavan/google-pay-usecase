package com.my.googlepay.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.my.googlepay.dto.UserCreatedRespDto;
import com.my.googlepay.dto.UserRegistrationDto;
import com.my.googlepay.dto.UserValidationViaPhoneNoRespDto;
import com.my.googlepay.exception.InvalidUserRegistrationException;
import com.my.googlepay.feignclient.GpayClient;
import com.my.googlepay.model.User;
import com.my.googlepay.repository.UserRepository;
import com.my.googlepay.service.UserService;

@Service
public class UserServiceImpl implements UserService {

		@Autowired
	UserRepository userRepository;
	
	@Autowired
	GpayClient client;

	@Override
	public UserCreatedRespDto registerCustomer(UserRegistrationDto userRegistrationDto) {
			
		ResponseEntity<UserValidationViaPhoneNoRespDto> userValidation = client.userValidationViaPhoneNumber(userRegistrationDto.getPhoneNumber());
		if(!userValidation.getStatusCode().is2xxSuccessful())
			throw new InvalidUserRegistrationException("Invalid Details Found:", userValidation);
		User user = new User();
		BeanUtils.copyProperties(userRegistrationDto, user);
		user = userRepository.save(user);
				
		return new UserCreatedRespDto(user.getUserID(), user.getFirstName(),
												user.getLastName(), user.getPhoneNumber());
	}

}
