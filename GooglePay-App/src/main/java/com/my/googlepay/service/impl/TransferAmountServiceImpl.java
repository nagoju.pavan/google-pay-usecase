package com.my.googlepay.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.my.googlepay.dto.AccountTransactionSuccessRespDto;
import com.my.googlepay.dto.TransferAmountViaPhoneNoReqDto;
import com.my.googlepay.exception.TransferAmountException;
import com.my.googlepay.exception.UserNotRegisteredException;
import com.my.googlepay.feignclient.GpayClient;
import com.my.googlepay.model.TransactionHistory;
import com.my.googlepay.model.User;
import com.my.googlepay.repository.TransactionHistoryRepository;
import com.my.googlepay.repository.UserRepository;
import com.my.googlepay.service.TransferAmountService;

@Service
public class TransferAmountServiceImpl implements TransferAmountService {

	@Autowired
	TransactionHistoryRepository transactionHistoryRepository;

	@Autowired
	GpayClient client;

	@Autowired
	UserRepository userRepository;

	@Override
	public AccountTransactionSuccessRespDto transferAmountThroughPhoneNumber(
			TransferAmountViaPhoneNoReqDto transferAmountWithPhoneNumberRequest) {
		validateIfThePhoneNumberIsRegisteredWithGpay(transferAmountWithPhoneNumberRequest);

		 callBankServiceForAmountTransaction(
				transferAmountWithPhoneNumberRequest);
				AccountTransactionSuccessRespDto transactionSuccessResponse = createAndSaveTransactionHistory(
				transferAmountWithPhoneNumberRequest);

		return transactionSuccessResponse;
	}

	@Override
	public List<TransactionHistory> getLatest10Transactions(String customerPhoneNumber) {

		List<TransactionHistory> transactionHistoryList = transactionHistoryRepository
				.findFirstByUserPhoneNumberOrderByTransactionDateDesc(customerPhoneNumber);

		return transactionHistoryList;
	}

	@Override
	public List<TransactionHistory> getLatestTransactions(String customerPhoneNumber) {
		Pageable pageable = PageRequest.of(0, 10);
		List<TransactionHistory> transactionHistoryList = transactionHistoryRepository
				.findByUserPhoneNumberOrderByTransactionDateDesc(customerPhoneNumber, pageable);

		return transactionHistoryList;
	}

	private ResponseEntity<AccountTransactionSuccessRespDto> callBankServiceForAmountTransaction(
			TransferAmountViaPhoneNoReqDto transferAmountWithPhoneNumberRequest) {
		ResponseEntity<AccountTransactionSuccessRespDto> transactionSuccess = client
				.transferMoneyThroughPhoneNumber(transferAmountWithPhoneNumberRequest);
		return transactionSuccess;
	}

	private AccountTransactionSuccessRespDto createAndSaveTransactionHistory(
			TransferAmountViaPhoneNoReqDto transferAmountViaPhoneNumberReq) {
		TransactionHistory debitTransaction = new TransactionHistory(
				transferAmountViaPhoneNumberReq.getFromPhoneNumber(),
				transferAmountViaPhoneNumberReq.getToPhoneNumber(),
				transferAmountViaPhoneNumberReq.getAmountToBeTransferred(), "DEBIT",
				transferAmountViaPhoneNumberReq.getComments());
		TransactionHistory creditTransaction = new TransactionHistory(
				transferAmountViaPhoneNumberReq.getToPhoneNumber(),
				transferAmountViaPhoneNumberReq.getFromPhoneNumber(),
				transferAmountViaPhoneNumberReq.getAmountToBeTransferred(), "CREDIT",
				transferAmountViaPhoneNumberReq.getComments());

		debitTransaction = transactionHistoryRepository.save(debitTransaction);
		creditTransaction = transactionHistoryRepository.save(creditTransaction);
		AccountTransactionSuccessRespDto transactionSuccessResponse = new AccountTransactionSuccessRespDto(
				debitTransaction.getTransactionID(), creditTransaction.getTransactionID());
		return transactionSuccessResponse;
	}

	private void validateIfThePhoneNumberIsRegisteredWithGpay(TransferAmountViaPhoneNoReqDto transferAmountRequest) {
		List<String> phoneNumbers = new ArrayList<>(
				Arrays.asList(transferAmountRequest.getFromPhoneNumber(), transferAmountRequest.getToPhoneNumber()));

		List<User> customerList = userRepository.findByPhoneNumberIn(phoneNumbers);

		if (customerList.size() < 2) {
			throw new UserNotRegisteredException(
					"Customer Registration with Gpay is mandatory for transferring the amount");
		}

	}
}
