package com.my.googlepay.service;

import java.util.List;

import com.my.googlepay.dto.AccountTransactionSuccessRespDto;
import com.my.googlepay.dto.TransferAmountViaPhoneNoReqDto;
import com.my.googlepay.model.TransactionHistory;

public interface TransferAmountService {
	public AccountTransactionSuccessRespDto transferAmountThroughPhoneNumber(
								TransferAmountViaPhoneNoReqDto transferAmountWithPhoneNumberRequest);
	
	public List<TransactionHistory> getLatest10Transactions(String customerPhoneNumber);
	public List<TransactionHistory> getLatestTransactions(String customerPhoneNumber);
}
