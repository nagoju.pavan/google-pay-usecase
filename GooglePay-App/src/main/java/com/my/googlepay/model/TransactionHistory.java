package com.my.googlepay.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class TransactionHistory {

	@Id @GeneratedValue
	private Long transactionID;
	
	
	private String userPhoneNumber;
	
	private String recepientPhoneNumber;

	private BigDecimal transactionAmount;
	private String transactionType;
	@Column	
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="yyyy-MM-dd")
	private Date transactionDate = new Date();
	private String comments;
	/**
	 * @return the transactionID
	 */
	public Long getTransactionID() {
		return transactionID;
	}
	/**
	 * @param transactionID the transactionID to set
	 */
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}
	/**
	 * @return the userPhoneNumber
	 */
	public String getUserPhoneNumber() {
		return userPhoneNumber;
	}
	/**
	 * @param userPhoneNumber the userPhoneNumber to set
	 */
	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}
	/**
	 * @return the recepientPhoneNumber
	 */
	public String getRecepientPhoneNumber() {
		return recepientPhoneNumber;
	}
	/**
	 * @param recepientPhoneNumber the recepientPhoneNumber to set
	 */
	public void setRecepientPhoneNumber(String recepientPhoneNumber) {
		this.recepientPhoneNumber = recepientPhoneNumber;
	}
	/**
	 * @return the transactionAmount
	 */
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	/**
	 * @param transactionAmount the transactionAmount to set
	 */
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @param transactionID
	 * @param userPhoneNumber
	 * @param recepientPhoneNumber
	 * @param transactionAmount
	 * @param transactionType
	 * @param transactionDate
	 * @param comments
	 */
	public TransactionHistory(Long transactionID, String userPhoneNumber, String recepientPhoneNumber,
			BigDecimal transactionAmount, String transactionType, Date transactionDate, String comments) {
		this.transactionID = transactionID;
		this.userPhoneNumber = userPhoneNumber;
		this.recepientPhoneNumber = recepientPhoneNumber;
		this.transactionAmount = transactionAmount;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.comments = comments;
	}
	/**
	 * 
	 */
	public TransactionHistory() {
		super();
		// TODO Auto-generated constructor stub
	}
		
	  public TransactionHistory(String customerPhoneNumber, String
	  recepientPhoneNumber, BigDecimal transactionAmount, String transactionType,
	  String comment) { super(); this.userPhoneNumber = customerPhoneNumber;
	  this.recepientPhoneNumber = recepientPhoneNumber; this.transactionAmount =
	  transactionAmount; this.transactionType = transactionType; this.comments =
	  comment;
	  }
}

	  
/*
 * public TransactionHistory(Long transactionID, String customerPhoneNumber,
 * String recepientPhoneNumber, BigDecimal transactionAmount, String
 * transactionType, Date transactionDate, String comment) { this.transactionID =
 * transactionID; this.userPhoneNumber = customerPhoneNumber;
 * this.recepientPhoneNumber = recepientPhoneNumber; this.transactionAmount =
 * transactionAmount; this.transactionType = transactionType;
 * this.transactionDate = transactionDate; this.comments = comment;
 * 
 * 
 * 
 * }
 */


