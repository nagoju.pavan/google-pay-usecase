package com.my.googlepay.exception;

import org.springframework.http.ResponseEntity;

import com.my.googlepay.dto.AccountTransactionSuccessRespDto;

public class TransferAmountException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TransferAmountException(ResponseEntity<AccountTransactionSuccessRespDto> transactionSuccess) {
		
		super(transactionSuccess.getBody().toString());
	}

}
