package com.my.googlepay.exception;

public class UserNotRegisteredException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UserNotRegisteredException(String errorMsg) {
		super(errorMsg);
	}

}
