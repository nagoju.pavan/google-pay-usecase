package com.my.googlepay.exception;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import feign.FeignException;

@ControllerAdvice
public class ControllerAdvisor {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
	}

	@ExceptionHandler(SQLException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> sqlException(SQLException sqlException) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(sqlException.getMessage());
	}

	@ExceptionHandler(InvalidUserRegistrationException.class)
	public ResponseEntity<String> invalidCustomerRegistrationDetailsException(
			InvalidUserRegistrationException invalidCustomerException) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidCustomerException.getMessage());
	}

	@ExceptionHandler(TransferAmountException.class)
	public ResponseEntity<String> transferAmountException(TransferAmountException transferAmountException) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(transferAmountException.getMessage());
	}

	@ExceptionHandler(UserNotRegisteredException.class)
	public ResponseEntity<String> customerNotRegisteredException(
			UserNotRegisteredException unregisteredCustomerException) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(unregisteredCustomerException.getMessage());
	}

	@ExceptionHandler(FeignException.class)
	public ResponseEntity<String> externalServiceCallException(FeignException feignException) {

		HttpStatus status = HttpStatus.resolve(feignException.status());
		if (status == null)
			status = HttpStatus.INTERNAL_SERVER_ERROR;

		return ResponseEntity.status(status).body(feignException.getMessage());
	}

}
