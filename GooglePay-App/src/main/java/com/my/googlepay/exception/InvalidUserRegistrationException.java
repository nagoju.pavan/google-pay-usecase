package com.my.googlepay.exception;

import org.springframework.http.ResponseEntity;

import com.my.googlepay.dto.UserValidationViaPhoneNoRespDto;

public class InvalidUserRegistrationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidUserRegistrationException(String errorMsg,
			ResponseEntity<UserValidationViaPhoneNoRespDto> userValidation) {		
		super(errorMsg);		
	}

	
}
