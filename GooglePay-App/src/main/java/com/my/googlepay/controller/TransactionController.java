package com.my.googlepay.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.my.googlepay.dto.AccountTransactionSuccessRespDto;
import com.my.googlepay.dto.TransferAmountViaPhoneNoReqDto;
import com.my.googlepay.model.TransactionHistory;
import com.my.googlepay.service.TransferAmountService;

@RestController
public class TransactionController {

	@Autowired
	TransferAmountService transferAmountService;

	@PutMapping("/transfer-via-phonenumbers")
	public ResponseEntity<AccountTransactionSuccessRespDto> transferMoneyThroughPhoneNumber(
			@Valid @RequestBody TransferAmountViaPhoneNoReqDto transferAmountWithPhoneNumberRequest) {
		AccountTransactionSuccessRespDto transactionResponse = transferAmountService
				.transferAmountThroughPhoneNumber(transferAmountWithPhoneNumberRequest);

		return ResponseEntity.status(HttpStatus.OK).body(transactionResponse);
	}

	@GetMapping("/phonenumber/{phoneNumber}/history")
	public ResponseEntity<List<TransactionHistory>> getLast10Transactions(@PathVariable String phoneNumber) {
		List<TransactionHistory> transactionHistoryList = transferAmountService.getLatestTransactions(phoneNumber);

		return ResponseEntity.status(HttpStatus.OK).body(transactionHistoryList);
	}
}
