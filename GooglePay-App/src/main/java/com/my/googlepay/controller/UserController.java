package com.my.googlepay.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.my.googlepay.dto.UserCreatedRespDto;
import com.my.googlepay.dto.UserRegistrationDto;
import com.my.googlepay.service.UserService;

@RestController
public class UserController {
	@Autowired
	UserService userService;

	@PostMapping("/registeruser")
	public ResponseEntity<UserCreatedRespDto> registerUser(
			@Valid @RequestBody UserRegistrationDto customerRegistrationDto) {
		UserCreatedRespDto userCreatedResp = userService.registerCustomer(customerRegistrationDto);
		return ResponseEntity.status(HttpStatus.OK).body(userCreatedResp);
	}

}
