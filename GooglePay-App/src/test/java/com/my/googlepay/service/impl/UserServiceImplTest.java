package com.my.googlepay.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.my.googlepay.dto.UserCreatedRespDto;
import com.my.googlepay.dto.UserRegistrationDto;
import com.my.googlepay.dto.UserValidationViaPhoneNoRespDto;
import com.my.googlepay.feignclient.GpayClient;
import com.my.googlepay.model.User;
import com.my.googlepay.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

	@InjectMocks
	private UserServiceImpl customerServiceImpl;
	
	@Mock
	private UserRepository customerRepository;
	
	@Mock
	private GpayClient client;
	
	
	@Test
	void registerCustomer_HappyPath() {
		when(client.userValidationViaPhoneNumber(anyString())).thenReturn(buildCustomerValidationSuccessResponse());
		when(customerRepository.save(any())).thenReturn(new User(1L, "Arya", "Stark", 19, "9874561235", "arya.stark@gmail.com",
			"arya@123"));
		UserRegistrationDto customerRegistrationRequest = buildCustomerRegistrationRequest();
		 UserCreatedRespDto customerCreatedResponse = customerServiceImpl.registerCustomer(customerRegistrationRequest);
		 assertEquals(customerRegistrationRequest.getPhoneNumber(), customerCreatedResponse.getPhoneNumber());
	}


	private UserRegistrationDto buildCustomerRegistrationRequest() {
		
		UserRegistrationDto customerRegistrationDto = new UserRegistrationDto("Arya", "Stark", 19, "9874561235", "arya.stark@gmail.com", "arya@123");		
		return customerRegistrationDto;
	}


	private ResponseEntity<UserValidationViaPhoneNoRespDto> buildCustomerValidationSuccessResponse() {
		UserValidationViaPhoneNoRespDto response = new UserValidationViaPhoneNoRespDto();
		response.setDoesCustomerExists(true);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}




}
