package com.my.googlepay.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.my.googlepay.dto.AccountTransactionSuccessRespDto;
import com.my.googlepay.dto.TransferAmountViaPhoneNoReqDto;
import com.my.googlepay.feignclient.GpayClient;
import com.my.googlepay.model.TransactionHistory;
import com.my.googlepay.model.User;
import com.my.googlepay.repository.TransactionHistoryRepository;
import com.my.googlepay.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
class TransferAmountServiceImplTest {

	@InjectMocks
	private TransferAmountServiceImpl transferAmountServiceImpl;
	
	@Mock
	private TransactionHistoryRepository transactionHistoryRepository;
	
	@Mock
	private GpayClient client;
	
	@Mock
	private UserRepository customerRepository;
	
	@Test
	void get10RecentTransaction() {
		
		List<TransactionHistory> transactionHistoryList = buildTransactionHistoryList();
		when(transactionHistoryRepository.findByUserPhoneNumberOrderByTransactionDateDesc(
				anyString(), any())).thenReturn(transactionHistoryList);
		List<TransactionHistory> latestTransactions = transferAmountServiceImpl.getLatestTransactions("9494074609");
		assertEquals(2, latestTransactions.size());
	}
	
	@Test
	void transferAmountThroughPhoneNumbersPositiveFlow() {
		
		List<User> customerList = buildCustomerList();
		when(customerRepository.findByPhoneNumberIn(any())).thenReturn(customerList);
		
		
		ResponseEntity<AccountTransactionSuccessRespDto> transferMoneySuccessResponse = buildTransferMoneySuccessResponse();
		when(client.transferMoneyThroughPhoneNumber(any())).thenReturn(transferMoneySuccessResponse);
		
		TransactionHistory debitTransaction = buildTransactionHistory("9087654321", "9187654321", "DEBIT", 1L);
		when(transactionHistoryRepository.save(any())).thenReturn(debitTransaction);
		
		TransferAmountViaPhoneNoReqDto requestdto = new TransferAmountViaPhoneNoReqDto("9087654321", "9187654321", BigDecimal.valueOf(1000), new Date(), "random comment");
		AccountTransactionSuccessRespDto transferAmountSuccessResponse = transferAmountServiceImpl.transferAmountThroughPhoneNumber(requestdto );
		assertEquals(1L, transferAmountSuccessResponse.getDebitTransactionID());
	}

	private TransactionHistory buildTransactionHistory(String fromPhoneNumber, String toPhoneNumber, String transactionType, Long transactionId) {
		TransactionHistory transactionHistory = new TransactionHistory(fromPhoneNumber, toPhoneNumber, BigDecimal.valueOf(1000), transactionType, "random comment");
		transactionHistory.setTransactionID(transactionId);
		return transactionHistory;
	}
	
	private ResponseEntity<AccountTransactionSuccessRespDto> buildTransferMoneySuccessResponse() {
		AccountTransactionSuccessRespDto successResponse = new AccountTransactionSuccessRespDto(1L, 2L);
		return ResponseEntity.status(HttpStatus.OK).body(successResponse);
	}

	private List<User> buildCustomerList() {
		List<User> customerList = new ArrayList<>();
		User customer1 = new User(1L, "Ram", "Kumar", 25, "9494074609", "ram.kumar@gmail.com", "ram@123");
		User customer2 = new User(2L, "Shyam", "Kumar", 26, "9187654321", "shyam.kumar@gmail.com", "shyam@123");
		
		customerList.add(customer1);
		customerList.add(customer2);
		
		return customerList;
	}

	private List<TransactionHistory> buildTransactionHistoryList() {
		List<TransactionHistory> transactionHistoryList = new ArrayList<>();
		TransactionHistory transaction1 = new TransactionHistory(1L,"9087654321", "9187654321", BigDecimal.valueOf(1000), "CREDIT", new Date(), "First Transaction");
		TransactionHistory transaction2 = new TransactionHistory(2L,"9087654321", "9187654321", BigDecimal.valueOf(2000), "DEBIT", new Date(),"Second Transaction");
		transactionHistoryList.add(transaction1);
		transactionHistoryList.add(transaction2);
		return transactionHistoryList;
	}
	

}
